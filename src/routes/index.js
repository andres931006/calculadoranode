const { Router } = require('express');
const calculator = require('../controller/index')
const router = Router();
const Request = require("request");

//routes
router.get('/',calculator.getAllOperations)
router.get('/add/:n1/:n2',calculator.getAdd)
router.get('/subtract/:n1/:n2',calculator.getSubtract)
router.post('/add',calculator.postAdd)
router.post('/subtract',calculator.postSubtract)
router.post('/multiply',calculator.postMultiply)
router.post('/divide',calculator.postDivide)


const getUserEs = () =>{
  
  Request.get("https://58c635049a514a9cad75be15e340f257.us-east-1.aws.found.io:9243/user", (error, response, body) => {
    if(error) {
      return console.dir(error);
    }
    console.dir(JSON.parse(body));
  });
}
router.get('/user', getUserEs)

module.exports = router;



