// Add this to the VERY top of the first file loaded in your app
var apm = require('elastic-apm-node').start({
  // Override service name from package.json
  // Allowed characters: a-z, A-Z, 0-9, -, _, and space
  serviceName: '',

  // Use if APM Server requires a token
  secretToken: 'd1tSFyM4XKDe5eFT9H',

  // Set custom APM Server URL (default: http://localhost:8200)
  serverUrl: 'https://2d914b6aca4345f5ac2eb0b4b147bb88.apm.us-east-1.aws.cloud.es.io:443'
})


